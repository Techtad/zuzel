addEventListener("DOMContentLoaded", function (event) {
    Game.pauseRace()
    Game.start()
})

var Game = {
    loadResources() {
        let asphaltImg = document.createElement("img")
        asphaltImg.src = "gfx/asphalt.png"
        asphaltImg.onload = function () { Patterns.Asphalt = ctx.createPattern(asphaltImg, "repeat") }
        let grassImg = document.createElement("img")
        grassImg.src = "gfx/grass.png"
        grassImg.onload = function () { Patterns.Grass = ctx.createPattern(grassImg, "repeat") }
        let motorImg = document.createElement("img")
        motorImg.src = "gfx/motor-transparent.png"
        motorImg.onload = function () { Images.Motor = motorImg }
    },

    create: function () {
        this.track = new RaceTrack()
        this.entities.push(this.track)

        this.players = []
        PlayersConfig.createPlayers()

        addEventListener("keydown", (function (event) { Input.keyPressed(event.key) }))
        addEventListener("keyup", (function (event) { Input.keyReleased(event.key) }))
    },

    start: function () {
        if (this.started) return
        this.started = true
        cvs = document.getElementById("canvas")
        ctx = cvs.getContext("2d")

        this.loadResources()

        PlayersConfig.init()
        Scores.init()

        this.entities = []
        this.create()

        this.maxLaps = 0
        this.track.setText("Pozostało okrążeń: " + Settings.LapGoal)
        this.livingPlayers = -1

        this.lastTime = new Date().getTime()
        requestAnimationFrame(this.update.bind(this))
    },

    update: function () {
        let delta = (new Date().getTime() - this.lastTime) / Settings.FrameTime
        this.lastTime = new Date().getTime()

        if (!this.racePaused) {
            for (e of this.entities) {
                e.update(delta)
                for (other of this.entities) {
                    if (other.id != e.id && e.collideWith(other)) e.onCollision(other)
                }
            }
            for (p of this.players) {
                if (!p) continue
                p.update(delta)
                for (other of this.entities) {
                    if (other.id != p.id && p.collideWith(other)) p.onCollision(other)
                }
            }
        }

        this.render()
    },

    render: function () {
        ctx.fillStyle = Patterns.Grass//"#22BB22"
        ctx.fillRect(0, 0, cvs.width, cvs.height)
        ctx.beginPath()

        for (e of this.entities) e.render()
        for (p of this.players) if (p) p.render()

        if (!this.paused)
            requestAnimationFrame(this.update.bind(this))
    },

    pause: function () {
        this.paused = true
    },

    resume: function () {
        this.paused = false
        this.lastTime = new Date().getTime()
        requestAnimationFrame(this.update.bind(this))
    },

    pauseRace: function () {
        this.racePaused = true
    },

    resumeRace: function () {
        this.racePaused = false
    },

    restart: function () {
        document.getElementById("lap-goal").disabled = true
        this.maxLaps = 0
        this.track.setText("Pozostało okrążeń: " + Settings.LapGoal)
        PlayersConfig.createPlayers()
    },

    lapCompleted: function (player) {
        if (player.fullLaps == Settings.LapGoal) {
            this.track.setText(`Gracz ${player.playerNum + 1} wygrał!`, player.color)
            this.endGame()
        } else {
            if (player.fullLaps > this.maxLaps) {
                this.maxLaps = player.fullLaps
                this.track.setText("Pozostało okrążeń: " + (Settings.LapGoal - this.maxLaps))
            }
        }
    },

    playerDied: function (player) {
        Scores.lastLaps[player.playerNum].innerText = "DNF"
        this.deadPlayers++
        if (this.playerCount > 1 && this.deadPlayers == this.playerCount - 1) {
            let winner
            for (let p of this.players) {
                if (!p) continue
                if (p.alive) winner = p
            }
            if (winner) this.track.setText(`Gracz ${winner.playerNum + 1} wygrał!`, winner.color)
            else alert("Błąd! Wszyscy umarli!")
            this.endGame()
        } else if (this.playerCount == 1) {
            this.track.setText(`Nikt nie wygrał!`, "#222222")
            this.endGame()
        }
    },

    endGame: function () {
        this.pauseRace()
        document.getElementById("lap-goal").disabled = false
    }
}