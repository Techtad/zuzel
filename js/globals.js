var cvs, ctx
var Settings = {
    FrameTime: 16,
    TrailLength: 192,
    TrailWidth: 4,
    PlayerSpeed: 3,
    TurnSpeed: 0.05,
    MotorcycleLength: 42,
    MotorcycleWidth: 24,
    EqualizePlayerStarts: false,
    RightKeysEnabled: false,
    LapGoal: 3
}
var Patterns = {}
var Images = {}