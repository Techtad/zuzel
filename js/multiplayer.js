var PlayersConfig = {
    playerData: [],

    init: function () {
        this.checkboxes = document.querySelectorAll(".player-checkbox")
        this.colors = document.querySelectorAll(".player-color")
        this.leftkeys = document.querySelectorAll(".key-select-left")
        this.rightkeys = document.querySelectorAll(".key-select-right")
        for (let i = 0; i < 4; i++) {
            this.playerData[i] = {}
        }
        this.eventHandlers()
        this.updateData()
    },

    eventHandlers() {
        for (let b of document.getElementsByTagName("input")) if (b.type == "checkbox") b.checked = (b.id == "player-1-enabled")
        document.getElementById("lap-goal").value = 3

        for (let box of this.checkboxes) {
            box.addEventListener("input", function (event) {
                let playerIndex = parseInt(event.target.attributes["playernum"].value)
                let value = event.target.checked
                PlayersConfig.playerData[playerIndex]["enabled"] = value

                PlayersConfig.colors[playerIndex].disabled = !value
                PlayersConfig.leftkeys[playerIndex].disabled = !value
                PlayersConfig.rightkeys[playerIndex].disabled = !value
                if (playerIndex < 3)
                    PlayersConfig.checkboxes[playerIndex + 1].disabled = !value
                if (playerIndex > 0)
                    PlayersConfig.checkboxes[playerIndex - 1].disabled = value

                if (value) {
                    event.target.parentNode.parentNode.removeAttribute("inactive")
                    Scores.scoreRows[playerIndex].style.display = ""
                }
                else {
                    event.target.parentNode.parentNode.setAttribute("inactive", true)
                    Scores.scoreRows[playerIndex].style.display = "none"
                }

                Game.pauseRace()
                PlayersConfig.createPlayers()
            })
        }
        for (let col of this.colors) {
            col.addEventListener("change", function (event) {
                let playerIndex = parseInt(event.target.attributes["playernum"].value)
                let value = event.target.value
                PlayersConfig.playerData[playerIndex]["color"] = value

                Game.pauseRace()
                PlayersConfig.createPlayers()
            })
        }
        for (let lkey of this.leftkeys) {
            lkey.addEventListener("keydown", function (event) {
                event.preventDefault()
                let playerIndex = parseInt(event.target.attributes["playernum"].value)
                let value = event.key
                event.target.placeholder = value
                PlayersConfig.playerData[playerIndex]["leftKey"] = value

                Game.pauseRace()
                PlayersConfig.createPlayers()
            })
        }
        document.getElementById("right-keys-enabled").addEventListener("input", function (event) {
            if (event.target.checked) {
                for (let td of document.getElementsByClassName("right-keys"))
                    td.style.visibility = "visible"
                Settings.RightKeysEnabled = true
            } else {
                for (let td of document.getElementsByClassName("right-keys"))
                    td.style.visibility = "hidden"
                Settings.RightKeysEnabled = false
            }
        })
        for (let rkey of this.rightkeys) {
            rkey.addEventListener("keydown", function (event) {
                event.preventDefault()
                let playerIndex = parseInt(event.target.attributes["playernum"].value)
                let value = event.key
                event.target.placeholder = value
                PlayersConfig.playerData[playerIndex]["rightKey"] = value

                Game.pauseRace()
                PlayersConfig.createPlayers()
            })
        }
    },

    updateData: function () {
        this.collectData(this.checkboxes, "enabled")
        this.collectData(this.colors, "color")
        this.collectData(this.leftkeys, "leftKey")
        this.collectData(this.rightkeys, "rightKey")
    },

    collectData: function (elements, key) {
        for (let el of elements) {
            let playerIndex = parseInt(el.attributes["playernum"].value)
            let value
            switch (key) {
                case "enabled":
                    value = el.checked
                    break
                case "color":
                    value = el.attributes.value.value
                    break
                case "leftKey": case "rightKey":
                    value = el.attributes.placeholder.value
            }
            this.playerData[playerIndex][key] = value
        }
    },

    createPlayers() {
        let playerCount = 0
        for (let i = 0; i < 4; i++) {
            let data = this.playerData[i]
            if (data.enabled) {
                let equalization = 2 * Math.PI * (cvs.height / 2 - cvs.height * (0.5 - 0.025 * i))
                if (!Settings.EqualizePlayerStarts) equalization *= 0
                Game.players[i] = new Player(cvs.width / 2 - (Settings.MotorcycleLength / 2) - equalization, cvs.height * (0.80 + (0.05 * (3 - i))), data.color, data.leftKey, data.rightKey, i)
                playerCount++
            } else
                Game.players[i] = null
            Scores.lapCounts[i].innerText = 0
            Scores.lastLaps[i].innerText = "-"
            Scores.bestLaps[i].innerText = "-"
        }
        Game.playerCount = playerCount
        Game.deadPlayers = 0
    }
}

var Scores = {
    init: function () {
        this.scoreRows = []
        this.lapCounts = []
        this.lastLaps = []
        this.bestLaps = []

        for (let i = 1; i <= 4; i++) {
            this.scoreRows[i - 1] = document.getElementById(`player-${i}-score`)
            this.lapCounts[i - 1] = document.getElementById(`player-${i}-laps`)
            this.lastLaps[i - 1] = document.getElementById(`player-${i}-last`)
            this.bestLaps[i - 1] = document.getElementById(`player-${i}-best`)
        }
    }
}