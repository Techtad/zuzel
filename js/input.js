var Input = {
    keys_down: [],

    keyPressed: function (key) {
        this.keys_down[key] = true
    },

    keyReleased: function (key) {
        this.keys_down[key] = false
    },

    isKeyDown: function (key) {
        return this.keys_down[key]
    }
}