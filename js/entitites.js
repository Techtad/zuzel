class Entity {
    constructor() { this.type = "ENTITY"; this.id = Game.entities.length }
    update(delta) { }
    render() { }
    collideWith(other) { return false }
    onCollision(other) { }
}

class Bike extends Entity {
    constructor(x, y, angle, speed, color) {
        super()
        this.type = "BIKE"
        this.x = x
        this.y = y
        this.angle = angle
        this.speed = speed
        this.color = color

        this.trail = []
    }

    update(delta) {
        super.update(delta)

        this.trail.push({ x: this.x, y: this.y })
        if (this.trail.length > Settings.TrailLength) this.trail.splice(0, 1)

        this.x += delta * this.speed * Math.cos(this.angle)
        this.y += delta * this.speed * Math.sin(this.angle)
    }

    render() {
        super.render()

        for (let i in this.trail) {
            let opacityHex = (Math.round(i / (Settings.TrailLength - 1) * 128)).toString(16)
            if (opacityHex.length == 1) opacityHex = "0" + opacityHex
            ctx.fillStyle = this.color + opacityHex
            //ctx.fillRect(this.trail[i].x - Settings.TrailWidth / 2, this.trail[i].y - Settings.TrailWidth / 2, Settings.TrailWidth, Settings.TrailWidth)
            ctx.beginPath()
            ctx.arc(this.trail[i].x, this.trail[i].y, Settings.TrailWidth, 0, Math.PI * 2)
            ctx.closePath()
            ctx.fill()
        }

        if (Images.Motor) {
            ctx.save()
            ctx.translate(this.x, this.y)
            ctx.rotate(this.angle)
            ctx.translate(-Settings.MotorcycleLength / 2, -Settings.MotorcycleWidth / 2)
            ctx.fillStyle = this.color
            ctx.fillRect(Settings.MotorcycleLength * 0.125, Settings.MotorcycleWidth * 0.267, Settings.MotorcycleLength * 0.8, Settings.MotorcycleWidth * 0.4)
            ctx.drawImage(Images.Motor, 0, 0, Settings.MotorcycleLength, Settings.MotorcycleWidth)
            ctx.restore()
        }
    }

    collideWith(other) {
        let hitX = this.x + Settings.MotorcycleLength / 2 * Math.cos(this.angle)
        let hitY = this.y + Settings.MotorcycleLength / 2 * Math.sin(this.angle)
        switch (other.type) {
            case "TRACK":
                let distLeftCirc = Math.hypot(hitX - cvs.height / 2, hitY - cvs.height / 2)
                let distRightCirc = Math.hypot(hitX - cvs.width + cvs.height / 2, hitY - cvs.height / 2)
                return distLeftCirc < cvs.height / 4 || distRightCirc < cvs.height / 4
                    || (hitX < cvs.height / 2 && distLeftCirc > cvs.height / 2) || (hitX > cvs.width - cvs.height / 2 && distRightCirc > cvs.height / 2)
                    || (hitX > cvs.height / 2 && hitX < cvs.width - cvs.height / 2 && hitY > cvs.height / 4 && hitY < cvs.height * 3 / 4)
                    || hitX < 0 || hitX > cvs.width || hitY < 0 || hitY > cvs.height
            default: break
        }

        return false
    }
}

class Player extends Bike {
    constructor(x, y, color, leftKey, rightKey, playerNum) {
        super(x, y, 0, Settings.PlayerSpeed, color)
        this.type = "PLAYER"
        this.leftKey = leftKey
        this.rightKey = rightKey

        this.lastX = x
        this.lapDist = 0
        this.laps = 0
        this.fullLaps = 0
        //this.lapStart = new Date().getTime()
        this.lapTime = 0
        this.bestLap = Infinity

        this.playerNum = playerNum

        this.alive = true
        this.started = false
    }

    update(delta) {
        if (!this.alive) {
            if (this.trail.length > 0) this.trail.splice(0, 1)
            return
        }
        super.update(delta)

        if (Input.isKeyDown(this.leftKey))
            this.angle -= Settings.TurnSpeed * delta
        if (Settings.RightKeysEnabled && Input.isKeyDown(this.rightKey))
            this.angle += Settings.TurnSpeed * delta

        let deltaX = this.lastX - this.x
        this.lastX = this.x

        if (!this.started && deltaX < 0 && this.x + Settings.MotorcycleLength / 2 * Math.cos(this.angle) > cvs.width / 2) this.started = true
        if (!this.started) return

        if (this.y < cvs.height / 4 && this.x > cvs.height / 2 && this.x < cvs.width - cvs.height / 2) {
            this.lapDist += deltaX
        } else if (this.y > cvs.height * 3 / 4 && this.x > cvs.height / 2 && this.x < cvs.width - cvs.height / 2) {
            this.lapDist -= deltaX
        }

        this.lapTime += delta * Settings.FrameTime

        this.laps = this.lapDist / (cvs.width - cvs.height) / 2

        let lastFull = this.fullLaps
        this.fullLaps = this.laps >= 0 ? Math.floor(this.laps) : Math.ceil(this.laps)
        if (lastFull != this.fullLaps) {
            //let lapTime = new Date().getTime() - this.lapStart
            //this.lapStart = new Date().getTime()

            Game.lapCompleted(this)

            Scores.lapCounts[this.playerNum].innerText = this.fullLaps
            Scores.lastLaps[this.playerNum].innerText = this.lapTime / 1000 + "s"
            if (this.lapTime < this.bestLap) this.bestLap = this.lapTime
            Scores.bestLaps[this.playerNum].innerText = this.bestLap / 1000 + "s"

            this.lapTime = 0
        }
    }

    onCollision(other) {
        if (this.alive && other.type == "TRACK") {
            this.alive = false
            Game.playerDied(this)
        }
    }
}

class RaceTrack extends Entity {
    constructor() {
        super()
        this.type = "TRACK"
        this.text = ""
    }

    render() {
        super.render()

        ctx.beginPath()

        ctx.arc(cvs.height / 2, cvs.height / 2, cvs.height / 2, Math.PI / 2, 3 * Math.PI / 2)
        ctx.arc(cvs.width - cvs.height / 2, cvs.height / 2, cvs.height / 2, -Math.PI / 2, Math.PI / 2)
        ctx.moveTo(cvs.height / 2, cvs.height)
        ctx.lineTo(cvs.width - cvs.height / 2, cvs.height)

        ctx.strokeStyle = "#000000"
        ctx.lineWidth = 5
        ctx.fillStyle = Patterns.Asphalt//"#FFFFFF"
        ctx.stroke()
        ctx.fill()

        ctx.closePath()

        ctx.beginPath()

        ctx.arc(cvs.height / 2, cvs.height / 2, cvs.height / 4, Math.PI / 2, 3 * Math.PI / 2)
        ctx.arc(cvs.width - cvs.height / 2, cvs.height / 2, cvs.height / 4, -Math.PI / 2, Math.PI / 2)
        ctx.moveTo(cvs.height / 2, cvs.height * 3 / 4)
        ctx.lineTo(cvs.width - cvs.height / 2, cvs.height * 3 / 4)

        ctx.strokeStyle = "#000000"
        ctx.lineWidth = 5
        ctx.fillStyle = Patterns.Grass//"#22BB22"
        ctx.stroke()
        ctx.fill()

        ctx.closePath()

        ctx.beginPath()
        ctx.strokeStyle = "#FFFFFF"
        ctx.lineWidth = 4
        ctx.moveTo(cvs.width / 2, cvs.height * 3 / 4)
        ctx.lineTo(cvs.width / 2, cvs.height)
        ctx.stroke()
        ctx.closePath()

        if (this.text && this.text.length > 0) {
            ctx.font = "48px Verdana"
            ctx.fillStyle = this.textColor
            ctx.fillText(this.text, cvs.width / 2 - (this.text.length * 12), cvs.height / 2 + 20)
        }
    }

    setText(t, color) {
        this.text = t
        if (color) this.textColor = color
        else this.textColor = "#EEEEEE"
    }
}